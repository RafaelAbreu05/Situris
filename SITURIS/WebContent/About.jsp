<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<tags:headerLogo title='About'/>
<body onload='showFocus()'>
<tags:headerAndMenu/> 

<body>
<h2>About SITURIS</h2>

<p>SITURIS provide tourist information services to it's users.</p>
<p>It integrates a map of routes, with information on various tours, organized by type of interest.
Each tour includes reference points. </p>
<p>All reference point can gather information about visits and events with tourist interest, so you can explore the every places you go.</p> 
<p>SITURIS also offers a rating system in order to help you choosing the route or places to visit.</p>

<p>Developers:
<ul>
	 <li>Ana Sampaio (<a
      href="mailto:pg20190@alunos.uminho.pt?subject=SITURIS">PG20190</a>)
     <li>André Pimenta (<a
      href="mailto:pg20189@alunos.uminho.pt?subject=SITURIS">PG20189</a>)
    <li>Andreia Silva (<a
      href="mailto:pg20190@alunos.uminho.pt?subject=SITURIS">A56837</a>)
    <li>Cedric Pimenta (<a
      href="mailto:pg19824@alunos.uminho.pt?subject=SITURIS">PG19824</a>)
    <li>Rafael Abreu (<a
      href="mailto:pg20978@alunos.uminho.pt?subject=SITURIS">PG20978</a>)
</ul>

 <tags:footer/>

</body>
</html>