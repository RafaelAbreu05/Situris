package hirondelle.situris.users.showaccount;

public class AccountStat {
  
  public Integer getNumRoteiros() {
    return fNumRoteiros;
  }
  public void setfNumRoteiros(Integer fNumRoteiros) {
    this.fNumRoteiros = fNumRoteiros;
  }
  public Integer getNumPtRef() {
    return fNumPtRef;
  }
  public void setfNumPtRef(Integer fNumPtRef) {
    this.fNumPtRef = fNumPtRef;
  }
  public Integer getNumVis() {
    return fNumVis;
  }
  public void setfNumVis(Integer fNumVis) {
    this.fNumVis = fNumVis;
  }
  public Integer getNumEvs() {
    return fNumEvs;
  }
  public void setfNumEvs(Integer fNumEvs) {
    this.fNumEvs = fNumEvs;
  }
  public Integer getNumEvas() {
    return fNumEvas;
  }
  public void setfNumEvas(Integer fNumEvas) {
    this.fNumEvas = fNumEvas;
  }
  public Integer getNumComps() {
    return fNumComps;
  }
  public void setfNumComps(Integer fNumComps) {
    this.fNumComps = fNumComps;
  }
  public Integer getNumProps() {
    return fNumProps;
  }
  public void setfNumProps(Integer fNumProps) {
    this.fNumProps = fNumProps;
  }
  public Integer getNumPats() {
    return fNumPats;
  }
  public void setfNumPats(Integer fNumPats) {
    this.fNumPats = fNumPats;
  }
  

  private Integer fNumRoteiros;
  private Integer fNumPtRef;
  private Integer fNumVis;
  private Integer fNumEvs;
  private Integer fNumEvas;
  private Integer fNumComps;
  private Integer fNumProps;
  private Integer fNumPats;
  
}
